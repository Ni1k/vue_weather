import { boot } from 'quasar/wrappers';
import YmapPlugin from 'vue-yandex-maps';
export default boot(({ app }) => {
  const settings = {
    apiKey: 'f35c292b-dc63-4087-9729-41c9780e2ca8', // Индивидуальный ключ API
    lang: 'ru_RU', // Используемый язык
    coordorder: 'latlong', // Порядок задания географических координат
    debug: false, // Режим отладки
    version: '2.1', // Версия Я.Карт
  };
  app.use(YmapPlugin, settings);
});
