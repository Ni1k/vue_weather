export interface WeatherStateInterface {
  weather: Array<unknown>;
}

function state(): WeatherStateInterface {
  return {
    weather: [],
  };
}

export default state;
