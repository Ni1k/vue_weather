import axios from 'axios';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { WeatherStateInterface } from './state';
const actions: ActionTree<WeatherStateInterface, StateInterface> = {
  someAction(context) {
    console.log(context);
  },
  async api_weather({ commit }, query = 'Krasnodar') {
    const options: Record<string, unknown> = {
      method: 'GET',
      url: 'https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily',
      params: { city: query },
      headers: {
        'X-RapidAPI-Key': '69acf0d401msh3e04a18cef9d7c8p1539c0jsn23457215ba70',
        'X-RapidAPI-Host': 'weatherbit-v1-mashape.p.rapidapi.com',
      },
    };

    const weather = await axios.request(options);
    console.log(weather);
    commit('setWeather', weather.data);
  },
};

export default actions;
