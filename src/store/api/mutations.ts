import { MutationTree } from 'vuex';
import { WeatherStateInterface } from './state';
import { useQuasar } from 'quasar';
const mutation: MutationTree<WeatherStateInterface> = {
  setWeather(state: WeatherStateInterface, value) {
    state.weather = value;
  },
};

export default mutation;
