import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { WeatherStateInterface } from './state';

const getters: GetterTree<WeatherStateInterface, StateInterface> = {
  someGetters(context) {
    return context;
  },
  getcity(state) {
    return state.weather.city_name;
  },
  getfirstday(state) {
    return state.weather;
  },
  getcoords(state) {
    if (state.weather && state.weather.lat)
      return [state.weather.lat, state.weather.lon];
    else return [];
  },
};

export default getters;
